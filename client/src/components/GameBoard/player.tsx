import React from "react";
import {PlayerTs} from "../../domain/player";
import {Colors, GameConfigTS} from "../../domain/config";
import {isALocker} from "../../domain/rules";

function defineColor(player: PlayerTs, colorsConfig: Colors, currentEpoch: number, lockDelay: number) {
    const red = "#bf616a"
    const green = "#a3be8c"
    const orange = "#d08770"
    switch (colorsConfig) {
        case Colors.ONE:
            return green
        case Colors.TWO:
            return player.isSuffocate ? red : green
        case Colors.THREE:
            if (isALocker(player, currentEpoch, lockDelay)) return red
            return player.isSuffocate ? orange : green
    }

}

export function drawPlayer(player: PlayerTs, playerNumber: number, canvasHeight: number, spacer: number, playerWidth: number, margin: number, currentEpoch: number, config: GameConfigTS) {
    const  colorsConfig = config.colors
    const lockDelay = config.lockDelay
    const offset = 33;
    const spacers = playerNumber === 0 ? 0 : spacer * playerNumber - 1
    const x = margin + (playerWidth) * playerNumber + spacers;
    const height = player.size
    const y = canvasHeight - height - offset
    const d = `M${x} ${y}h${playerWidth}v${height}H${x}z`
    const color = defineColor(player, colorsConfig, currentEpoch, lockDelay);
    return <g>
        <path fill={color} strokeWidth={0} d={d}/>
        <text x={x} y={canvasHeight - 4} fill="#bf616a" style={{fontFamily: 'sans-serif', fontSize: 28 + 'px'}}>
            {player.name}
        </text>
    </g>
}