export type GameConfigTS = {
    lockDelay: number,
    maxMove: number,
    show: boolean,
    penaliseFactor: number,
    level: Levels,
    typescript: string,
    colors: Colors,
}

export type GameConfigJava = {
    lockDelay: number,
    maxMove: number,
    show: boolean,
    penaliseFactor: number,
    level: string,
    colors: string
}

export enum Levels {
    TRAINING = "Entraînement",
    EASY = "Facile",
    MEDIUM = "Moyen",
    HARD = "Difficile",
}

export enum Colors {
    ONE = "Une",
    TWO = "Deux",
    THREE = "Trois",
}

export const defaultConfig: GameConfigTS = {
    lockDelay: 20,
    maxMove: 1,
    show: true,
    penaliseFactor: 0.1,
    level: Levels.TRAINING,
    typescript: "force type checking",
    colors: Colors.ONE,
};

export function getLockDelay(numberOfPlayers: number, levelFactor: number) {
    return numberOfPlayers < 3 ? levelFactor : levelFactor * (numberOfPlayers - 2);
}

export function configFromLevel(value: Levels, numberOfPlayers: number): GameConfigTS {
    const commonConfig = {...defaultConfig, level: value}
    switch (value) {
        case Levels.TRAINING:
            return {...commonConfig, lockDelay: 20, maxMove: 1}
        case Levels.EASY:
            return {...commonConfig, lockDelay: getLockDelay(numberOfPlayers, 10), maxMove: 1}
        case  Levels.MEDIUM:
            return {...commonConfig, lockDelay: getLockDelay(numberOfPlayers, 5), maxMove: 2}
        case  Levels.HARD:
            return {...commonConfig, lockDelay: getLockDelay(numberOfPlayers, 3), maxMove: 3}
    }
}

export function string2Level(s: string): Levels {
    // @ts-ignore
    return Levels[s]
}

export function string2Colors(s: string): Colors {
    // @ts-ignore
    return Colors[s]
}

export function level2string(l: Levels): string {
    return Object.entries(Levels)
        .filter(([k, v]) => v == l)
        .map(([k, v]) => k)
        [0]
}

export function colors2string(c: Colors): string {
    return Object.entries(Colors)
        .filter(([k, v]) => v == c)
        .map(([k, v]) => k)
        [0]
}

export function colors2label(c: Colors): string {
    return Object.entries(Colors)
        .filter(([k, v]) => v == c)
        .map(([k, v]) => v)
        [0]
}

export function level2label(l: Levels): string {
    return Object.entries(Levels)
        .filter(([k, v]) => v == l)
        .map(([k, v]) => v)
        [0]
}




export function configTypeFromJava(java: GameConfigJava): GameConfigTS {
    return {
        lockDelay: java.lockDelay,
        maxMove: java.maxMove,
        show: java.show,
        penaliseFactor: java.penaliseFactor,
        level: string2Level(java.level),
        typescript: "force type checking",
        colors: string2Colors(java.colors)
    }
}

export function configType2Java(type: GameConfigTS): GameConfigJava {
    return {
        lockDelay: type.lockDelay,
        maxMove: type.maxMove,
        show: type.show,
        penaliseFactor: type.penaliseFactor,
        level: level2string(type.level),
        colors: colors2string(type.colors)
    }
}