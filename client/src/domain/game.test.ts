import {computePollutionLevel, computeScoreGame, INITIAL_SCORE} from "./game";
import {PlayerTs} from "./player";

describe("Game", () => {
    describe("computePollutionLevel", () => {
        it("should be 0 when no players", () => {
            expect(computePollutionLevel([])).toBe(0)
        })
        it("should be maximum size of each players", () => {
            const defaultPlayer: PlayerTs = {name: "A", isSuffocate: false, size: 200, locked: false, startSuffocate: 0}
            const players = [{...defaultPlayer, size: 150}, {...defaultPlayer, size: 250}]
            expect(computePollutionLevel(players)).toBe(250)
        })
    })
    describe("computeScore", ()=>{
        it('should be the minimum', function () {
            expect(computeScoreGame(100, 90)).toBe(90)
            expect(computeScoreGame(90, 100)).toBe(90)
        })
        it('should be INITIAL_SCORE only if new score', function () {
            expect(computeScoreGame(INITIAL_SCORE, 90)).toBe(INITIAL_SCORE)
            expect(computeScoreGame(90, INITIAL_SCORE)).toBe(90)
        })
    })
})