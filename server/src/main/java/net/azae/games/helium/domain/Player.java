package net.azae.games.helium.domain;

public class Player {
    public static final int INITIAL_SIZE = 200;
    private String name;
    private int size = INITIAL_SIZE;

    public Player() {
        name = NameGenerator.random();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object other) {
        Player right = (Player) other;
        return name.equals(right.getName()) && size == right.getSize();
    }

}
