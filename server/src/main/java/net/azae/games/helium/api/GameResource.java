package net.azae.games.helium.api;

import net.azae.games.helium.domain.Game;
import net.azae.games.helium.domain.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Random;

import static net.azae.games.helium.api.MemoryDB.games;

@Path("games")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GameResource {
    private static Logger log = LoggerFactory.getLogger(GameResource.class);

    @POST
    public Game create() {
        Game game = new Game();
        int idNum = new Random().nextInt(9999);
        String id = String.format("%04d", idNum);
        game.setId(id);
        games.put(id, game);
        log.info("new Game {} created", id);
        return game;
    }

    @GET
    public Collection<Game> all() {
        log.debug("returning Games {}", games.keySet());
        return games.values();
    }

    @GET
    @Path("{id}")
    public Game read(@PathParam("id") String id) {
        if(games.containsKey(id)) {
            Game game = games.get(id);
            log.debug("returning Game by id {}", game);
            return game;
        } else {
            log.debug("Game "+ id + " not found");
            throw new NotFoundException();
        }
    }
}
