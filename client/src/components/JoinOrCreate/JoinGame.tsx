import React from "react";
import {Dispatch} from "redux";
import {connect, ConnectedProps} from "react-redux";
import styles from "./index.module.css"
import appStyles from "../App/App.module.css"
import {getApiBaseUrl, requestNewPlayer} from "./index";
import {State} from "../../state";
import {HeliumAction} from "../../reducer";
import {GameJava} from "../../domain/game";

function mapStateToProps(state: State) {
    return {};
}

async function joinGame(dispatch: Dispatch<HeliumAction>): Promise<void> {
    const input = document.getElementById("gameId") as HTMLInputElement
    const response = await fetch(getApiBaseUrl() + 'games/' + input.value);
    if (response.ok) {
        const game = await response.json() as GameJava;
        dispatch({type: "CREATE_GAME", game: game})
        await requestNewPlayer(dispatch, game)
    }
}

function mapDispatchJoinGame(dispatch: Dispatch<HeliumAction>) {
    return {
        joinGame: () => joinGame(dispatch),
    }
}

const joinGameConnector = connect(mapStateToProps, mapDispatchJoinGame)
type PropsFromRedux = ConnectedProps<typeof joinGameConnector>

function JoinGame(props: PropsFromRedux): JSX.Element {
    return (
        <div>
            <h2>Joueurs</h2>
            <p>
                <button onClick={props.joinGame} className={[appStyles.bgGreen, styles.commonFormItem].join(" ")} tabIndex={1}>
                    <em>Rejoindre</em> la partie n°
                </button>
                <input id="gameId" type="text" placeholder="Game ID" tabIndex={0}
                       className={[styles.input, styles.commonFormItem].join(" ")}/>
            </p>
        </div>
    );
}

export default joinGameConnector(JoinGame)


