package net.azae.games.helium.websocket;

import org.junit.jupiter.api.Test;

import javax.websocket.DecodeException;
import javax.websocket.EncodeException;

import static net.azae.games.helium.websocket.Helper.sampleMessage;
import static net.azae.games.helium.websocket.Helper.samplePlayer;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameMessageEncoderTest {
    @Test
    public void should_encode_full_GameMessage() throws DecodeException, EncodeException {
        String actual = new GameMessageEncoder().encode(sampleMessage("Game name", 200, samplePlayer("player", 132)));
        String expected = "" +
                "{" +
                "\"player\":{\"name\":\"player\",\"size\":132}," +
                "\"score\":200," +
                "\"gameId\":\"Game name\"," +
                "\"config\":null" +
                "}";
        assertEquals(expected, actual);
    }

    @Test
    public void should_encode_GameMessage_without_player() throws DecodeException, EncodeException {
        String actual = new GameMessageEncoder().encode(sampleMessage("Game name", 200, null));
        String expected = "" +
                "{" +
                "\"player\":null," +
                "\"score\":200," +
                "\"gameId\":\"Game name\"," +
                "\"config\":null" +
                "}";
        assertEquals(expected, actual);
    }

}