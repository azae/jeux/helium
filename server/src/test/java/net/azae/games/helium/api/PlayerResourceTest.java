package net.azae.games.helium.api;


import net.azae.games.helium.domain.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PlayerResourceTest {
    @Test
    public void should_create_game_with_initial_size() {
        PlayerResource playerResource = new PlayerResource();
        Player player = playerResource.create(new GameId("AAAA"));
        assertNotNull(player.getName());
        assertEquals(200, player.getSize());
    }

}