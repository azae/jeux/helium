package net.azae.games.helium.websocket;

import net.azae.games.helium.domain.Player;

public class Helper {
    public static GameMessage sampleMessage(String gameName, int score, Player player) {
        GameMessage message = new GameMessage();
        message.setGameId(gameName);
        message.setScore(score);
        message.setPlayer(player);
        return message;
    }

    public static Player samplePlayer(String playerName, int playerSize) {
        Player player = new Player();
        player.setName(playerName);
        player.setSize(playerSize);
        return player;
    }
}
