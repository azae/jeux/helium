import {PlayerJava, PlayerTs} from "./player";

export type GameJava = {
    id: string;
    players: PlayerJava[];
    score: number;
}
export type GameTs = {
    id: string;
    score: number;
    pollutionLevel: number;
}

export const INITIAL_SCORE = 1000;

export function computeScoreGame(newScore: number, oldScore: number): number {
    if (newScore == INITIAL_SCORE) {
        return INITIAL_SCORE
    } else {
        return oldScore >= newScore ? newScore : oldScore
    }
}

export function computePollutionLevel(players: PlayerTs[]) {
    return Math.max(...players.map((p) => p.size), 0)
}

export function mergeGame(newGame: GameJava, oldGame: GameTs | undefined): GameTs {
    if (oldGame)
        return {
            id: newGame.id,
            score: computeScoreGame(newGame.score, oldGame.score),
            pollutionLevel: 0
        }
    else
        return {
            id: newGame.id,
            score: newGame.score,
            pollutionLevel: 0
        }
}

export function updatePollutionLevel(game: GameTs, players: PlayerTs[]): GameTs {
    return {
        ...game,
        pollutionLevel: computePollutionLevel(players)
    }
}

export function updateGameScore(game: GameTs, score: number): GameTs {
    return {
        ...game,
        score: computeScoreGame(score, game.score)
    };
}