package net.azae.games.helium.api;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/v1/")
public class Api extends ResourceConfig {

    public Api() {
        packages("net.azae.games.helium");
        //register(LoggingFilter.class);
    }

}
