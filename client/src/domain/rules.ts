import {PlayerTs} from "./player";
import {moveSize} from "../reducer";

export function isALocker(player: PlayerTs, currentEpoch: number, lockDelay: number) {
    if (player.isSuffocate) {
        return (currentEpoch - player.startSuffocate) > lockDelay;
    }
    return false
}

export function isPlayerLocked(ISuffocate: boolean, players: PlayerTs[], currentEpoch: number, lockDelay: number): boolean {
    if (ISuffocate)
        return false
    return players
        .filter((p) => isALocker(p, currentEpoch, lockDelay))
        .length > 0
}

export function isEndOfGame(players: PlayerTs[]): boolean {
    if (players.filter(p => p.size != 0).length == 0) {
        console.log("End of game")
        return true
    }
    return false
}

export function getNewSizeForMoveDown(player: PlayerTs, maxMove: number) {
    const newSize = player.size - moveSize(maxMove);
    return newSize >= 0 ? newSize : 0;
}