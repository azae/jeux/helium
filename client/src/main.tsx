import React from 'react'
import ReactDOM from 'react-dom'

import {Provider} from 'react-redux'
import {App} from "./components/App/App";
import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {heliumReducer} from "./reducer";
import reduxWebsocket from '@giantmachines/redux-websocket';
import {connect} from "@giantmachines/redux-websocket/dist";

export function getWsUrl(location: Location) {
    const protocol = location.protocol === 'http:' ? 'ws' : 'wss';
    return `${protocol}://${location.hostname}:${location.port}/play`;
}

export let websocket: WebSocket;
const reduxWebsocketMiddleware = reduxWebsocket({
    reconnectInterval: 1000,
    reconnectOnClose: true,
    onOpen: (socket: WebSocket) => {
        websocket = socket
    }
});

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    heliumReducer,
    composeEnhancers(applyMiddleware(thunk, reduxWebsocketMiddleware,))
)

export function tick() {
    store.dispatch({type: "TICK"})
    setTimeout(tick, 250);
}

// @ts-ignore
store.dispatch(connect(getWsUrl(window.location)))

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('container')
)