import {Colors, colors2string, getLockDelay, level2string, Levels, string2Colors, string2Level} from "./config";

describe("GameConfig", () => {
    describe("Levels", () => {
        it("should convert string to enum", () => {
            expect(string2Level("MEDIUM")).toBe(Levels.MEDIUM)
            expect(string2Level("EASY")).toBe(Levels.EASY)
        })
        it("should convert enum to string", () => {
            expect(level2string(Levels.HARD)).toBe("HARD")
            expect(level2string(Levels.EASY)).toBe("EASY")
        })
    })
    describe("Colors", () => {
        it("should convert string to enum", () => {
            expect(string2Colors("ONE")).toBe(Colors.ONE)
            expect(string2Colors("THREE")).toBe(Colors.THREE)
        })
        it("should convert enum to string", () => {
            expect(colors2string(Colors.ONE)).toBe("ONE")
            expect(colors2string(Colors.TWO)).toBe("TWO")
        })
    })
    describe("getLockDelay", () => {
        it("should be levelFactor when numbersOfPlayer is under 3", () => {
            expect(getLockDelay(1, 7)).toBe(7)
        })
        it("should be greater than levelFactor when numbersOfPlayer is greater 3", () => {
            expect(getLockDelay(10, 20)).toBeGreaterThan(20)
        })
    })
})