import {initialState, State} from "./state";
import {newPlayer, newPlayerFromJava, PlayerJava, PlayerTs, updatePlayerFromJava} from "./domain/player";
import {GameJava, GameTs, mergeGame, updateGameScore, updatePollutionLevel} from "./domain/game";
import {GameMessage} from "./domain/gameMessage";
import {Colors, configFromLevel, configTypeFromJava, Levels} from "./domain/config";
import {ScoreEntry} from "./domain/scoreBoard";

export  type HeliumAction =
    { type: "CREATE_GAME", game: GameJava }
    | { type: "CREATE_PLAYER", player: PlayerJava }
    | { type: "MOVE_DOWN", newSize: number }
    | { type: "MOVE_UP", newSize: number }
    | { type: "REDUX_WEBSOCKET::MESSAGE", payload: any }
    | { type: "REDUX_WEBSOCKET::CLOSED", payload: any }
    | { type: "TICK" }
    | { type: "UPDATE_CONFIGURATION_LOCK_DELAY", value: string }
    | { type: "UPDATE_CONFIGURATION_MAX_MOVE", value: string }
    | { type: "UPDATE_CONFIGURATION_PENALIZE_FACTOR", value: string }
    | { type: "UPDATE_LEVEL", value: Levels }
    | { type: "UPDATE_COLORS", value: Colors }
    | { type: "CLICK_ON_GAME" }

function extractPlayersFromGameJava(game: GameJava): PlayerTs[] {
    return game.players.map(p => newPlayerFromJava(p))
}

function updatePlayerInPlayers(players: PlayerTs[], player: PlayerTs): PlayerTs[] {
    if (players.filter(p => p.name === player.name).length === 0) return [...players, player]
    return players.map((p) => {
        if (p.name === player.name) return player
        return p
    })
}

function computeStartSuffocate(player: PlayerTs, isSuffocate: boolean, epoch: number): number {
    if (player.isSuffocate)
        return player.startSuffocate;
    else {
        if (isSuffocate) {
            return epoch;
        } else
            return 0;
    }
}

function updatePlayerSizeAndSuffocate(player: PlayerTs, pollutionLevel: number, size: number, epoch: number): PlayerTs {
    const isSuffocate = player.size < pollutionLevel;
    return {
        ...player,
        size: size,
        isSuffocate: isSuffocate,
        startSuffocate: computeStartSuffocate(player, isSuffocate, epoch)
    };
}

export function moveSize(max: number) {
    return Math.floor((Math.random() * max) + 3);
}

function updateSuffocation(players: PlayerTs[], game: GameTs, epoch: number): PlayerTs[] {
    return players.map((p) => updatePlayerSizeAndSuffocate(p, game.pollutionLevel, p.size, epoch));
}

function computeEffect(game: GameTs, p: PlayerTs, penalizeFactor: number) {
    return 1 + Math.floor((game.pollutionLevel - p.size) * penalizeFactor);
}

function extractPlayerFromName(players: PlayerTs[], name: string): PlayerTs {
    const oldPlayer = players.filter((p) => p.name === name)[0]
    return oldPlayer ? oldPlayer : newPlayer(name);
}

function move_down(state: State, action: { type: "MOVE_DOWN"; newSize: number }): State {
    const playersDown = updatePlayerInPlayers(state.players, updatePlayerSizeAndSuffocate(state.me, state.game.pollutionLevel, action.newSize, state.tickEpoch));

    return {
        ...state,
        players: playersDown,
        game: updatePollutionLevel(state.game, playersDown),
        me: updatePlayerSizeAndSuffocate(state.me, state.game.pollutionLevel, action.newSize, state.tickEpoch),
        scoreBoard: updateScoreBoard(state.config.level, state.game.pollutionLevel, state.game.score, state.scoreBoard, state.config.colors)
    }
}

function updateLevel(state: State, action: { type: "UPDATE_LEVEL"; value: Levels }) {
    return {
        ...state,
        config: configFromLevel(action.value, state.players.length)
    }
}

function updateColors(state: State, action: { type: "UPDATE_COLORS"; value: Colors }) {
    return {
        ...state,
        config: {...state.config, colors: action.value}
    }
}

function mustUpdateScore(pollutionLevel: number, gameLevel: Levels) {
    return pollutionLevel == 0 && gameLevel != Levels.TRAINING;
}

function isSameBoardItem(s: ScoreEntry, scoreBoardItem: ScoreEntry) {
    return s.level === scoreBoardItem.level && s.colors === scoreBoardItem.colors;
}

function replaceScoreEntryWithBetterScore(scoreBoard: ScoreEntry[], scoreBoardItem: ScoreEntry) {
    return scoreBoard.map((s) => {
        if (isSameBoardItem(s, scoreBoardItem)) {
            if (s.score < scoreBoardItem.score) {
                return scoreBoardItem
            }
        }
        return s
    })
}

function addNewScoreEntry(scoreBoard: ScoreEntry[], scoreBoardItem: ScoreEntry) {
    return [...scoreBoard, scoreBoardItem]
}

export function updateScoreBoard(gameLevel: Levels, pollutionLevel: number, score: number, scoreBoard: ScoreEntry[], gameColor: Colors) {
    if (mustUpdateScore(pollutionLevel, gameLevel)) {
        const currentScore: ScoreEntry = {level: gameLevel, score: score, colors: gameColor}
        if (scoreBoard.filter((s) => isSameBoardItem(s, currentScore)).length == 0) {
            return addNewScoreEntry(scoreBoard, currentScore);
        }
        return replaceScoreEntryWithBetterScore(scoreBoard, currentScore);
    } else {
        return scoreBoard
    }
}

function receive_message(state: State, action: { type: "REDUX_WEBSOCKET::MESSAGE"; payload: any }) {
    const message = JSON.parse(action.payload.message) as GameMessage;
    const playersFromMsg = updatePlayerInPlayers(state.players, updatePlayerFromJava(message.player, extractPlayerFromName(state.players, message.player.name)));
    const newGame = updatePollutionLevel(updateGameScore(state.game, message.score), playersFromMsg);
    const me = message.player.name === state.me.name ? updatePlayerFromJava(message.player, extractPlayerFromName(state.players, message.player.name)) : state.me;
    const scoreBoard = updateScoreBoard(state.config.level, newGame.pollutionLevel, newGame.score, state.scoreBoard, state.config.colors)
    console.log(message)
    return {
        ...state,
        game: newGame,
        players: updateSuffocation(playersFromMsg, newGame, state.tickEpoch),
        config: configTypeFromJava(message.config),
        me,
        scoreBoard
    }
}

function computePollutionEffectOnScoreWithPenalizeFactor(state: State) {
    return [...state.players.values()]
        .filter((p) => p.isSuffocate)
        .map(p => computeEffect(state.game, p, state.config.penaliseFactor))
        .reduce((acc, e) => acc + e, 0);
}

function computePollutionEffectOnScoreWithTime(players: PlayerTs[], epoch: number) {
    const numberOfSuffocatedPeoples = players
        .filter((p) => p.isSuffocate)
        .length;
    return numberOfSuffocatedPeoples >= 1 && epoch % 3 == 0 ? 1 : 0;
}


export function heliumReducer(state = initialState, action: HeliumAction): State {
    switch (action.type) {
        case "CREATE_GAME":
            return {
                ...state,
                game: updatePollutionLevel(mergeGame(action.game, state.game), extractPlayersFromGameJava(action.game)),
                players: extractPlayersFromGameJava(action.game),
                screen_JoinOrCreate: "Hidden",
                screen_Board: "Visible",
            }
        case "CREATE_PLAYER":
            const playersWithMe = updatePlayerInPlayers(state.players, updatePlayerFromJava(action.player, state.me));
            return {
                ...state,
                players: playersWithMe,
                game: updatePollutionLevel(state.game, playersWithMe),
                me: updatePlayerFromJava(action.player, state.me),
            }
        case "MOVE_DOWN":
            return move_down(state, action);
        case "MOVE_UP":
            const playersUp = updatePlayerInPlayers(state.players, updatePlayerSizeAndSuffocate(state.me, state.game.pollutionLevel, action.newSize, state.tickEpoch));
            return {
                ...state,
                players: playersUp,
                game: updatePollutionLevel(state.game, playersUp),
                me: updatePlayerSizeAndSuffocate(state.me, state.game.pollutionLevel, action.newSize, state.tickEpoch),
            }
        case "REDUX_WEBSOCKET::MESSAGE":
            return receive_message(state, action);
        case 'REDUX_WEBSOCKET::CLOSED':
            console.log(action)
            return initialState
        case "TICK":
            const pollutionEffect = computePollutionEffectOnScoreWithTime(state.players, state.tickEpoch)
            return {
                ...state,
                game: {...state.game, score: state.game.score - pollutionEffect},
                tickEpoch: state.tickEpoch + 1
            }
        case "UPDATE_CONFIGURATION_LOCK_DELAY":
            return {
                ...state,
                config: {...state.config, lockDelay: +action.value}
            }
        case "UPDATE_CONFIGURATION_MAX_MOVE":
            return {
                ...state,
                config: {...state.config, maxMove: +action.value}
            }
        case "UPDATE_CONFIGURATION_PENALIZE_FACTOR":
            return {
                ...state,
                config: {...state.config, penaliseFactor: +action.value}
            }
        case "UPDATE_LEVEL":
            return updateLevel(state, action);
        case "UPDATE_COLORS":
            return updateColors(state, action);
        default :
            console.log("Warning, action not handle")
            console.log(action)
            return state
    }
}