import {Dispatch} from "redux";
import {connect, ConnectedProps} from "react-redux";
import React from "react";
import JoinGame from "./JoinGame";
import NewGame from "./CreateGame";
import styles from "./index.module.css"
import appStyle from "../App/App.module.css"
import {HeliumAction} from "../../reducer";
import {PlayerJava} from "../../domain/player";
import {GameJava} from "../../domain/game";
import {sendMessage} from "../../domain/gameMessage";
import {isVisible, State} from "../../state";
import {configType2Java, defaultConfig} from "../../domain/config";

export function getApiBaseUrl() {
    return `${window.location.protocol}//${window.location.hostname}:${window.location.port}/v1/`;
}

function mapStateToProps(state: State) {
    return {isVisible: isVisible(state.screen_JoinOrCreate)}
}

function mapDispatchNewGame(dispatch: Dispatch<HeliumAction>) {
    return {}
}

const connector = connect(mapStateToProps, mapDispatchNewGame)

type PropsFromRedux = ConnectedProps<typeof connector>

export async function requestNewPlayer(dispatch: Dispatch<any>, game: GameJava): Promise<HeliumAction> {
    const response = await fetch(getApiBaseUrl() + 'players/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({gameId: game.id})
    });
    const player = await response.json() as PlayerJava;
    dispatch(sendMessage({gameId: game.id, score: game.score, player, config: configType2Java(defaultConfig)}));
    return dispatch({type: "CREATE_PLAYER", player})
}

function JoinOrCreate(props: PropsFromRedux) {
    const visible = props.isVisible ? appStyle.visible : appStyle.hidden
    const className = [visible, styles.popin].join(' ')
    return (<div className={className}><JoinGame/><NewGame/></div>);
}

export default connector(JoinOrCreate)
