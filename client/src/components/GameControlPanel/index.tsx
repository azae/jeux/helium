import React from "react";
import {connect, ConnectedProps} from "react-redux";
import {Dispatch} from "redux";
import style from "./index.module.css"
import {isVisible, State} from "../../state";
import {HeliumAction} from "../../reducer";
import GameConfig from "./GameConfig";
import GameButtons from "./GameButtons";
import GameLevel from "./GameLevel";
import {Levels} from "../../domain/config";

function mapStateToProps(state: State) {
    return {
        game: state.game,
        me: state.me,
        infoAreVisible: ! isVisible(state.screen_JoinOrCreate),
        scoreIsVisible: state.config.level != Levels.TRAINING
    }
}

function mapDispatchNewGame(dispatch: Dispatch<HeliumAction>) {
    return {}
}

const connector = connect(mapStateToProps, mapDispatchNewGame)

type PropsFromRedux = ConnectedProps<typeof connector>

function GameControlPanel(props: PropsFromRedux) {
    const infoVisible = props.infoAreVisible ? style.visible : style.hidden
    const allBarClassName = [infoVisible, style.head].join(' ')
    const scoreVisible = props.scoreIsVisible ? style.visible : style.hidden
    const scoreClassName = [scoreVisible, style.cardInfo].join(' ')
    return (
        <div className={allBarClassName}>
            <div className={style.card}>
                <div className={style.cardLabel}>ID de jeu</div>
                <div className={style.cardInfo}>{props.game.id}</div>
            </div>
            <div className={style.card}>
                <div className={style.cardLabel}>Moi</div>
                <div className={style.cardInfo}>{props.me.name}</div>
            </div>
            <div className={style.card}>
                <div className={style.cardLabel}>Score</div>
                <div className={scoreClassName}>{props.game.score}</div>
            </div>
            <GameConfig/>
            <GameLevel/>
            <GameButtons/>
        </div>
    )

}

export default connector(GameControlPanel)