import {PlayerTs} from "./domain/player";
import {GameTs} from "./domain/game";
import {defaultConfig, GameConfigTS} from "./domain/config";
import {ScoreEntry} from "./domain/scoreBoard";

export type Visible = "Visible" | "Hidden"

export type HelpState = {
    clickToFocus: "Todo" | "Done"
}


export type State = {
    game: GameTs,
    players: PlayerTs[]
    me: PlayerTs
    screen_JoinOrCreate: Visible
    screen_Board: Visible,
    tickEpoch: number,
    config: GameConfigTS,
    scoreBoard: ScoreEntry[],
    helpState: HelpState
}

export const initialState: State = {
    game: {id: "", score: 0, pollutionLevel: 0},
    players: [],
    me: {name: "", isSuffocate: false, locked: false, size: 0, startSuffocate: 0},
    screen_JoinOrCreate: "Visible",
    screen_Board: "Hidden",
    tickEpoch: 0,
    config: defaultConfig,
    scoreBoard: [],
    helpState: {clickToFocus: "Todo"}
}

export function isVisible(visible: Visible) {
    return visible == "Visible";
}