import React from 'react';
import GameControlPanel from "../GameControlPanel";
import JoinOrCreate from "../JoinOrCreate";
import GameBoard from "../GameBoard";
import {tick} from "../../main";
import ScoreBoard from "../ScoreBoard";

export function App() {
    tick()
    return (
        <div>
            <JoinOrCreate/>
            <GameBoard/>
            <GameControlPanel/>
        </div>
    )
}
