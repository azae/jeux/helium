var path = require('path');

module.exports = {
    // Change to your "entry-point".
    entry: './src/main',
    output: {
        path: path.resolve(__dirname, '../server/src/main/webapp'),
        filename: 'main.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.scss']
    },
    module: {
        rules: [{
            test: /\.(ts|js)x?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        }, {
            test: /\.css$/,
            loader: 'style-loader',
        }, {
            test: /\.css$/,
            loader: 'css-loader',
            options: {
                modules: true,
            },
        }, {
            test: /\.svg$/,
            use: ['@svgr/webpack'],
        }],
    }
};
