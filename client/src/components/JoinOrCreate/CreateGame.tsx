import React from "react";
import {connect, ConnectedProps} from "react-redux";
import {Dispatch} from "redux";
import styles from "./index.module.css";
import {getApiBaseUrl, requestNewPlayer} from "./index";
import {State} from "../../state";
import {HeliumAction} from "../../reducer";
import {GameJava} from "../../domain/game";
import appStyles from "../App/App.module.css";

function mapStateToProps(state: State) {
    return {};
}

async function requestNewGame(dispatch: Dispatch<HeliumAction>): Promise<void> {
    const response = await fetch(getApiBaseUrl() + 'games/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    });
    const game = await response.json() as GameJava;
    dispatch({type: "CREATE_GAME", game})
    await requestNewPlayer(dispatch, game)
}

function mapDispatchNewGame(dispatch: Dispatch<HeliumAction>) {
    return {
        asyncNewGame: () => requestNewGame(dispatch),
    }
}

const newGameConnector = connect(mapStateToProps, mapDispatchNewGame)
type PropsFromRedux = ConnectedProps<typeof newGameConnector>

function CreateGame(props: PropsFromRedux) {
    return (
        <div>
            <h2>Animateur</h2>
            <button onClick={props.asyncNewGame} className={[appStyles.bgGreen, styles.commonFormItem].join(" ")}>
                <em>Initier</em> une partie
            </button>
            <div className={styles.help}>Pour obtenir le numéro de la partie à communiquer aux autres joueurs</div>
        </div>
    );
}

export default newGameConnector(CreateGame)
