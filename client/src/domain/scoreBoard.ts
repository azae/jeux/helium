import {Colors, Levels} from "./config";

export type ScoreEntry = {
    level: Levels,
    colors: Colors,
    score: number
}