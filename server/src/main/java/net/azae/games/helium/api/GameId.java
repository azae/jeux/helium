package net.azae.games.helium.api;

public class GameId {
    private String gameId;

    public GameId() {
    }

    public GameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
