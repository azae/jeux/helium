package net.azae.games.helium.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.azae.games.helium.websocket.GameMessage;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class GameMessageEncoder implements Encoder.Text<GameMessage> {
    @Override
    public String encode(GameMessage gameMessage) throws EncodeException {
        try {
            return new ObjectMapper().writeValueAsString(gameMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new EncodeException(e, "Canot convert Game message");
        }
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
