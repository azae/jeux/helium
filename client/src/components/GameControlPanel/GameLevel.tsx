import {State} from "../../state";
import {Dispatch} from "redux";
import {HeliumAction} from "../../reducer";
import {connect, ConnectedProps} from "react-redux";
import style from "./index.module.css"
import React, {ChangeEvent} from "react";
import {level2string, Levels, string2Level} from "../../domain/config";


function mapStateToProps(state: State) {
    return {level: state.config.level}
}

function mapDispatchNewLevel(dispatch: Dispatch<HeliumAction>) {
    return {
        updateLevel: (v: ChangeEvent<HTMLSelectElement>) => {
            dispatch({type: "UPDATE_LEVEL", value: string2Level(v.target.value)})
        },
    }
}

const connector = connect(mapStateToProps, mapDispatchNewLevel)
type PropsFromRedux = ConnectedProps<typeof connector>

function buildOptions() {
    return Object.entries(Levels)
        .map(([k, v]) => <option value={k}>{v}</option>)
}

function GameLevel(props: PropsFromRedux) {
    return (
        <div className={style.card}>
            <div className={style.cardLabel}>Niveau</div>
            <div className={style.cardInfo}>
                <select className={style.select} onChange={props.updateLevel} value={level2string(props.level)}>
                    {buildOptions()}
                </select>
            </div>
        </div>
    )
}

export default connector(GameLevel)