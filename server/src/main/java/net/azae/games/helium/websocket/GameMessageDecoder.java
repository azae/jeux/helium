package net.azae.games.helium.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.azae.games.helium.websocket.GameMessage;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.IOException;

public class GameMessageDecoder implements Decoder.Text<GameMessage> {
    @Override
    public GameMessage decode(String s) throws DecodeException {
        try {
            return new ObjectMapper().readValue(s, GameMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new DecodeException(s, "Can't decode", e);
        }
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
