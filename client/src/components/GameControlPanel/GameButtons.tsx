import {State} from "../../state";
import {Dispatch} from "redux";
import {HeliumAction} from "../../reducer";
import {connect, ConnectedProps} from "react-redux";
import style from "./index.module.css"
import appStyle from "../App/App.module.css"
import React, {ChangeEvent} from "react";
import {sendMessage} from "../../domain/gameMessage";
import {convertPlayerToJava, INITIAL_SIZE, PlayerTs} from "../../domain/player";
import {INITIAL_SCORE} from "../../domain/game";
import {configType2Java} from "../../domain/config";

function mapStateToProps(state: State) {
    return {
        config: state.config
    }
}

function mapDispatchNewGame(dispatch: Dispatch<HeliumAction>) {
    return {
        restartGame: () => {
            console.log("Restart ....")
            // @ts-ignore
            dispatch((dispatch: Dispatch<any>, getState: any) => {
                const state : State = getState();
                state.players.forEach((p: PlayerTs) => {
                        console.log("send player " + p.name)
                        dispatch(sendMessage({
                            config: configType2Java(state.config),
                            score: INITIAL_SCORE,
                            gameId: state.game.id,
                            player: {...convertPlayerToJava(p), size: INITIAL_SIZE}
                        }))
                    })
                }
            )
        },
        reloadPage: () => {
            window.location.reload();
        },
    }
}

const connector = connect(mapStateToProps, mapDispatchNewGame)
type PropsFromRedux = ConnectedProps<typeof connector>

function GameButtons(props: PropsFromRedux) {
    return (<>
        <div className={style.card}>
            <div className={style.cardInfo}>
                <button className={[style.cardButton, appStyle.bgGreen].join(" ")} onClick={props.restartGame}>
                    <em>Rejouer</em> la partie
                </button>
            </div>
        </div>
        <div className={style.card}>
            <div className={style.cardInfo}>
                <button className={[style.cardButton, appStyle.bgGreen].join(" ")} onClick={props.reloadPage}>
                    <em>Réinitialisez</em> le nombre de joueurs
                </button>
            </div>
        </div>
    </>)
}

export default connector(GameButtons)