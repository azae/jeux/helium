import {getNewSizeForMoveDown, isPlayerLocked} from "./rules";
import {PlayerTs} from "./player";

function suffocatePlayerFrom(startSuffocate: number) {
    return {name: "A", isSuffocate: true, locked: false, size: 100, startSuffocate: startSuffocate};
}

function breathePlayer(name: string) {
    return {name: name, isSuffocate: false, locked: false, size: 100, startSuffocate: 0};
}

describe("Rules", () => {
    describe("player is locked", () => {
        it("when exist one other player suffocate from to many time", () => {
            const currentEpoch = 100;
            const lockDelay = 4;
            const longTime = currentEpoch - (lockDelay + 2);
            expect(isPlayerLocked(false, [suffocatePlayerFrom(longTime)], currentEpoch, lockDelay)).toBeTruthy()
        })
    })
    describe("player is not locked", () => {
        it("when all other player suffocate from short time", () => {
            const currentEpoch = 100;
            const lockDelay = 4;
            const shortTime = currentEpoch - (lockDelay - 2);
            expect(isPlayerLocked(false, [suffocatePlayerFrom(shortTime)], currentEpoch, lockDelay)).toBeFalsy()
        })
        it("when no player suffocate", () => {
            expect(isPlayerLocked(false, [breathePlayer("A")], 100, 4)).toBeFalsy()
        })
        it("when i suffocate", () => {
            const currentEpoch = 100;
            const lockDelay = 4;
            const longTime = currentEpoch - (lockDelay + 2);
            expect(isPlayerLocked(true, [suffocatePlayerFrom(longTime)], currentEpoch, lockDelay)).toBeFalsy()
        })
    })
    describe("player move down", () => {
        it("it can't move under 0", () => {
            const player: PlayerTs = {name: "A", size: 1, isSuffocate: false, locked: false, startSuffocate: 0}
            const newSize = getNewSizeForMoveDown(player, 200);
            expect(newSize).toBe(0)
        })
    })
})
