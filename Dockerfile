FROM debian
RUN apt-get update \
    && apt-get install -y \
        openjdk-11-jdk-headless \
        npm \
        maven \
    && apt-get clean
COPY server/pom.xml /app/server/
WORKDIR /app
RUN cd server && mvn install
COPY client/package.json  client/package-lock.json /app/client/
RUN cd client && npm install
COPY . /app
RUN cd client && npm run bundle
CMD cd server && mvn jetty:run
