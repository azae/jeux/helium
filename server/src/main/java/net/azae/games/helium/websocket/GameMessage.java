package net.azae.games.helium.websocket;

import net.azae.games.helium.domain.Player;

public class GameMessage {
    private Player player;
    private int score;
    private String gameId;
    private GameConfig config;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        String gameInfo = "Game ID: " + this.gameId + ", score: " + this.score;
        if (this.config != null)
            gameInfo += ", level: " + config.getLevel();
        if (this.player != null) {
            return gameInfo + ", player: " + player.getName() + " (" + player.getSize() + ")";
        }
        return gameInfo + ", player: No Player";
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        GameMessage right = (GameMessage) other;
        return gameId.equals(right.getGameId())
                && score == right.getScore()
                && isSamePlayer(right)
                && isSameConfig(right);
    }

    private boolean isSameConfig(GameMessage right) {
        return (player == null && right.getPlayer() == null) || (config == null && right.getConfig() == null) || config.equals(right.getConfig());
    }

    private boolean isSamePlayer(GameMessage right) {
        return (player == null && right.getPlayer() == null) || player.equals(right.getPlayer());
    }

    public GameConfig getConfig() {
        return config;
    }

    public void setConfig(GameConfig config) {
        this.config = config;
    }
}
