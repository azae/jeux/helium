package net.azae.games.helium.api;

import net.azae.games.helium.domain.Game;
import net.azae.games.helium.domain.Player;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("players")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PlayerResource {

    @POST
    public Player create(GameId gameId) {
        Player player = new Player();
        synchronized (MemoryDB.games) {
            Game game = MemoryDB.games.get(gameId.getGameId());
            if (game != null) game.addPlayer(player);
        }
        return player;
    }
}
