package net.azae.games.helium.websocket;

public class GameConfig {
    private int lockDelay;
    private int maxMove;
    private boolean show;
    private float penaliseFactor;
    private String level;
    private String colors;

    public float getPenaliseFactor() {
        return penaliseFactor;
    }

    public void setPenaliseFactor(float penaliseFactor) {
        this.penaliseFactor = penaliseFactor;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public int getMaxMove() {
        return maxMove;
    }

    public void setMaxMove(int maxMove) {
        this.maxMove = maxMove;
    }

    public int getLockDelay() {
        return lockDelay;
    }

    public void setLockDelay(int lockDelay) {
        this.lockDelay = lockDelay;
    }


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameConfig that = (GameConfig) o;

        if (lockDelay != that.lockDelay) return false;
        if (maxMove != that.maxMove) return false;
        if (show != that.show) return false;
        if (Float.compare(that.penaliseFactor, penaliseFactor) != 0) return false;
        if (level != null ? !level.equals(that.level) : that.level != null) return false;
        return colors != null ? colors.equals(that.colors) : that.colors == null;
    }

}
