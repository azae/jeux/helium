package net.azae.games.helium.websocket;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint(
        value = "/play",
        encoders = {GameMessageEncoder.class},
        decoders = {GameMessageDecoder.class})
public class GameWebSocketServer {
    private final static Logger logger = LoggerFactory.getLogger(GameWebSocketServer.class);
    private static final Map<Session, String> sessions = Collections.synchronizedMap(new HashMap<>());

    @OnOpen
    public void open(Session session) {
        logger.debug("Open connections");
        synchronized(session) {
            sessions.put(session, "No Name");
        }
    }

    @OnClose
    public void close(Session session) {
        logger.debug("Close connections");
        synchronized (session) {
            sessions.remove(session);
        }
    }

    @OnMessage
    public void handleMessage(GameMessage message, Session session) {
        logger.debug("handleMessage " + message);
        synchronized (sessions) {
            if (sessions.get(session).equals("No Name"))
                sessions.put(session, message.getGameId());
            List<Session> sessions = extractGameSessions(message.getGameId());
            logger.info("Game : "+message.getGameId()+", sending message to "+ sessions.size() + " players.");
            for (Session localSession : sessions) {
                localSession.getAsyncRemote().sendObject(message);
            }
        }
    }

    public List<Session> extractGameSessions(String gameName) {
        return sessions.keySet().stream()
                .filter(session -> sessions.get(session).equals(gameName))
                .filter(Session::isOpen)
                .collect(Collectors.toList());
    }
}