import {State} from "../../state";
import {Dispatch} from "redux";
import {HeliumAction, moveSize} from "../../reducer";
import {connect, ConnectedProps} from "react-redux";
import React from "react";
import styles from "./index.module.css"
import {drawPlayer} from "./player";
import {convertPlayerToJava, INITIAL_SIZE} from "../../domain/player";
import {sendMessage} from "../../domain/gameMessage";
import {getNewSizeForMoveDown, isEndOfGame, isPlayerLocked} from "../../domain/rules";
import {configType2Java} from "../../domain/config";
import ScoreBoard from "../ScoreBoard";

type Size = {
    width: number,
    height: number
}

function mapStateToProps(state: State) {
    return {
        game: state.game,
        players: state.players,
        gameIsVisible: state.screen_Board,
        me: state.me,
        config: state.config,
        currentEpoch: state.tickEpoch,
    }
}

function move(dispatch: Dispatch<any>, event: React.KeyboardEvent<any>) {
    switch (event.key) {
        case 'ArrowDown':
            dispatch((dispatch: Dispatch<any>, getState: any) => {
                    const state: State = getState()
                    const isLocked = isPlayerLocked(state.me.isSuffocate, state.players, state.tickEpoch, state.config.lockDelay);
                    if (!isLocked) {
                        const player = state.me
                        const newSize = getNewSizeForMoveDown(player, state.config.maxMove);
                        dispatch({type: "MOVE_DOWN", newSize})
                        dispatch(sendMessage({
                            gameId: state.game.id,
                            score: state.game.score,
                            player: convertPlayerToJava({...state.me, size: newSize}),
                            config: configType2Java(state.config)
                        }))
                    }
                }
            )
            break;
        case 'ArrowUp':
            dispatch((dispatch: Dispatch<any>, getState: any) => {
                    const state: State = getState()
                    const endOfGame = isEndOfGame(state.players)
                    if (!endOfGame) {
                        const player = state.me
                        const newSize = player.size + moveSize(state.config.maxMove);
                        dispatch({type: "MOVE_UP", newSize})
                        dispatch(sendMessage({
                            gameId: state.game.id,
                            score: state.game.score,
                            player: convertPlayerToJava({...state.me, size: newSize}),
                            config: configType2Java(state.config)
                        }))
                    }
                }
            )
            break;
    }

}

function mapDispatchGameMap(dispatch: Dispatch<HeliumAction>) {
    return {
        move: (event: React.KeyboardEvent<any>) => move(dispatch, event),
        clicked: (event: React.MouseEvent<any>) => {
            dispatch({type: "CLICK_ON_GAME"})
        }
    }
}

const connector = connect(mapStateToProps, mapDispatchGameMap)

type PropsFromRedux = ConnectedProps<typeof connector>

function getCanvasWidth(margin: number, spacer: number, playerWidth: number, numberOfPlayers: number) {
    const spacers = numberOfPlayers === 0 ? 0 : spacer * (numberOfPlayers - 1)
    return spacers + playerWidth * numberOfPlayers + margin * 2;
}

function drawPollutionLevel(mapSize: Size, pollutionLevel: number) {
    const {height, width} = mapSize
    const d = `M0 ${height - pollutionLevel - 5 - 33}h${width}v5H0z`
    return <g>
        <path fill="#b48ead" strokeWidth={0} d={d}/>
    </g>
}

function drawLine(mapSize: Size, level: number): JSX.Element {
    const {height, width} = mapSize
    const size = level == 0 ? 3 : 1
    const y = height - (level * 25) - INITIAL_SIZE - 5 - 33;
    const d = `M14 ${y}h${width}v${size}H14z`
    return <g>
        <text x={0} y={y + 4} fill="#eceff4" style={{fontFamily: 'sans-serif', fontSize: 9 + 'px'}}>
            {level}
        </text>
        <path fill="#eceff4" strokeWidth={0} d={d}/>
    </g>
}

function drawGrid(mapSize: Size): JSX.Element[] {
    let i: number
    let output = []
    for (i = -7; i <= 10; i++) {
        output.push(drawLine(mapSize, i))
    }
    return output
}

function drawZeroPollution(mapSize: Size) {
    const {height: canvasHeight, width: canvasWidth} = mapSize
    const d = `M0 ${canvasHeight - 33}h${canvasWidth}v1H0z`
    return <g>
        <path fill="#a3be8c" strokeWidth={0} d={d}/>
    </g>
}

function sun(pollutionLevel: number, mapHeight: number) {
    const scaleFactor = (mapHeight - pollutionLevel - 33) / 380
    const transform = `translate(16, 0) scale(${scaleFactor})`
    return (
        <g transform={transform}>
            <path
                d="M136.671 191.43c0 20.756-16.823 37.58-37.58 37.58-20.756 0-37.606-16.824-37.606-37.58 0-20.757 16.824-37.607 37.606-37.607 20.757.026 37.58 16.85 37.58 37.606zm-38.23-47.451a48 48 0 0114.922 2.37c-5.99-12.631-3.36-26.33-3.36-26.33 8.542-32.632-16.199-27.762-16.199-27.762 18.57 6.303 2.579 22.684 2.579 22.684-17.501 15.652-18.126 26.876-16.46 32.762a47.583 47.583 0 0118.517-3.724zm-58.754-.677c0 22.97 7.604 31.07 13.074 33.934 3.515-10.912 10.833-20.105 20.391-26.043-12.683-4.271-20.6-14.662-20.6-14.662-18.62-28.1-31.486-6.433-31.486-6.433 17.058-9.636 18.62 13.204 18.62 13.204zm15.417 69.379a47.861 47.861 0 01-4.661-20.679c0-4.27.573-8.438 1.614-12.37-11.927 5.834-24.767 4.115-24.767 4.115C-5.758 177.054.466 201.456.466 201.456c5.26-18.881 22.501-3.828 22.501-3.828 15.288 15.287 26.017 16.511 32.137 15.053zm84.693-45.107c6.146-11.146 16.771-17.058 16.771-17.058 30.47-14.454 10.834-30.262 10.834-30.262 7.11 18.282-15.704 16.563-15.704 16.563-25.913-3.724-34.116 5.078-36.668 10.13 10.417 3.803 19.167 11.173 24.767 20.627zm56.643 12.37c-5.26 18.882-22.501 3.829-22.501 3.829-16.303-16.303-27.423-16.59-33.283-14.74 3.724 6.823 5.834 14.636 5.834 22.943 0 3.23-.313 6.407-.938 9.48 11.745-5.469 24.09-3.802 24.09-3.802 33.023 6.719 26.798-17.71 26.798-17.71zm-37.528 58.623c-.234-23.933-8.542-31.642-14.063-34.116-2.943 10.99-9.688 20.418-18.725 26.824 12.292 4.297 20.001 14.194 20.001 14.194 18.907 27.918 31.564 6.12 31.564 6.12-17.006 9.792-18.777-13.022-18.777-13.022zM98.44 240c-3.62 0-7.136-.39-10.521-1.172 4.687 12.292 1.927 24.767 1.927 24.767-9.454 32.397 15.417 28.23 15.417 28.23-18.386-6.823-1.927-22.761-1.927-22.761 20.132-17.006 18.855-29.012 16.772-34.22-6.51 3.307-13.881 5.156-21.668 5.156zm-41.226-23.387c-5.13 11.902-15.47 18.881-15.47 18.881-29.116 16.98-8.203 31.07-8.203 31.07-8.647-17.605 14.245-17.84 14.245-17.84 23.36 1.328 31.773-6.068 34.794-11.407-10.678-3.724-19.637-11.146-25.366-20.704z"
                fill="#ebcb8b"
            />
        </g>)
}

function clouds(pollutionLevel: number, mapSize: Size): JSX.Element[] {
    const {height, width} = mapSize

    const scaleFactor = (pollutionLevel - 33) / 97
    const y = height - pollutionLevel - 25
    let translate = 16;
    let clouds = [];
    if (y > (height - 33 - 25 - 5))
        // too small to draw
        return []
    while (translate < width) {
        clouds.push(cloud(`translate(${translate}, ${y}) scale(${scaleFactor})`))
        translate += Math.abs(150 * scaleFactor)
        console.log(y)
    }
    return clouds
}

function cloud(transform: string): JSX.Element {
    return (
        <g transform={transform}>
            <path
                d="m 104.29822,24.4962 c 0,13.52888 -13.79395,24.49622 -30.80965,24.49622 -17.01571,0 -30.80966,-10.96734 -30.80966,-24.49622 C 42.67891,10.96733 56.47286,0 73.48857,0 c 17.0157,0 30.80965,10.96733 30.80965,24.4962 z"
                id="prefix__path2844"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 102.92794,25.34 c 0,13.43025 -13.23431,24.31762 -29.55965,24.31762 -16.32535,0 -29.55966,-10.88737 -29.55966,-24.31762 0,-13.43026 13.23431,-24.31763 29.55966,-24.31763 16.32534,0 29.55965,10.88737 29.55965,24.31763 z"
                id="prefix__path2844-3"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 105.56093,55.55842 c 0,12.2736 -11.19345,22.2233 -25.00127,22.2233 -13.80783,0 -25.00128,-9.9497 -25.00128,-22.2233 0,-12.27363 11.19345,-22.22337 25.00128,-22.22337 13.80782,0 25.00127,9.94974 25.00127,22.22337 z"
                id="prefix__path2844-9"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 125.2589,38.38582 c 0,12.2736 -11.19345,22.2234 -25.00127,22.2234 -13.80783,0 -25.00128,-9.9498 -25.00128,-22.2234 0,-12.27362 11.19345,-22.22336 25.00128,-22.22336 13.80782,0 25.00127,9.94974 25.00127,22.22336 z"
                id="prefix__path2844-5"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 69.7005,45.45688 c 0,12.27364 -11.19345,22.22334 -25.00127,22.22334 -13.80783,0 -25.00128,-9.9497 -25.00128,-22.22334 0,-12.27362 11.19345,-22.22336 25.00128,-22.22336 13.80782,0 25.00127,9.94974 25.00127,22.22336 z"
                id="prefix__path2844-11"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 72.11958,46.59002 c 0,12.2736 -11.47328,22.2233 -25.62628,22.2233 -14.153,0 -25.62627,-9.9497 -25.62627,-22.2233 0,-12.27364 11.47327,-22.22338 25.62627,-22.22338 14.153,0 25.62628,9.94974 25.62628,22.22338 z"
                id="prefix__path2844-11-1"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 39.90102,64.90232 c 0,9.9026 -8.93215,17.9302 -19.95051,17.9302 C 8.93214,82.83252 0,74.80492 0,64.90232 c 0,-9.9026 8.93214,-17.9302 19.95051,-17.9302 11.01836,0 19.95051,8.0276 19.95051,17.9302 z"
                id="prefix__path2844-1"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 40.46168,65.32922 c 0,9.3048 -8.77225,16.8479 -19.59337,16.8479 -10.82112,0 -19.59338,-7.5431 -19.59338,-16.8479 0,-9.3049 8.77226,-16.848 19.59338,-16.848 10.82112,0 19.59337,7.5431 19.59337,16.848 z"
                id="prefix__path2844-1-76"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 125.69101,40.51856 c 0,12.86536 -11.39333,23.29476 -25.44771,23.29476 -14.05438,0 -25.44771,-10.4294 -25.44771,-23.29476 0,-12.86535 11.39333,-23.29478 25.44771,-23.29478 14.05438,0 25.44771,10.42943 25.44771,23.29478 z"
                id="prefix__path2844-5-4"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 150.00764,45.96192 c 0,12.2737 -11.19345,22.2234 -25.00127,22.2234 -13.80783,0 -25.00128,-9.9497 -25.00128,-22.2234 0,-12.27358 11.19345,-22.22332 25.00128,-22.22332 13.80782,0 25.00127,9.94974 25.00127,22.22332 z"
                id="prefix__path2844-2"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 149.26243,45.87572 c 0,11.5833 -11.2734,20.9733 -25.17984,20.9733 -13.90645,0 -25.17985,-9.39 -25.17985,-20.9733 0,-11.58327 11.2734,-20.97337 25.17985,-20.97337 13.90644,0 25.17984,9.3901 25.17984,20.97337 z"
                id="prefix__path2844-2-2"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 41.41624,79.29702 c 0,6.5552 -5.201,11.8693 -11.61676,11.8693 -6.41575,0 -11.61675,-5.3141 -11.61675,-11.8693 0,-6.5553 5.201,-11.8693 11.61675,-11.8693 6.41576,0 11.61676,5.314 11.61676,11.8693 z"
                id="prefix__path2844-1-7"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 41.77076,78.55432 c 0,6.4566 -5.12105,11.6907 -11.43818,11.6907 -6.31714,0 -11.43819,-5.2341 -11.43819,-11.6907 0,-6.4566 5.12105,-11.6908 11.43819,-11.6908 6.31713,0 11.43818,5.2342 11.43818,11.6908 z"
                id="prefix__path2844-1-7-2"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 83.84264,75.76142 c 0,12.2737 -11.19345,22.2234 -25.00127,22.2234 -13.80783,0 -25.00128,-9.9497 -25.00128,-22.2234 0,-12.2736 11.19345,-22.2233 25.00128,-22.2233 13.80782,0 25.00127,9.9497 25.00127,22.2233 z"
                id="prefix__path2844-4"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 83.97924,75.18442 c 0,12.0144 -10.93019,21.7539 -24.41325,21.7539 -13.48308,0 -24.41327,-9.7395 -24.41327,-21.7539 0,-12.0144 10.93019,-21.754 24.41327,-21.754 13.48306,0 24.41325,9.7396 24.41325,21.754 z"
                id="prefix__path2844-4-2"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 138.89595,65.65992 c 0,12.2736 -11.19345,22.2234 -25.00127,22.2234 -13.80783,0 -25.00128,-9.9498 -25.00128,-22.2234 0,-12.2736 11.19345,-22.22335 25.00128,-22.22335 13.80782,0 25.00127,9.94975 25.00127,22.22335 z"
                id="prefix__path2844-8"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 137.566,65.38462 c 0,11.8052 -11.21344,21.3752 -25.04592,21.3752 -13.83248,0 -25.04591,-9.57 -25.04591,-21.3752 0,-11.8051 11.21343,-21.37511 25.04591,-21.37511 13.83248,0 25.04592,9.57001 25.04592,21.37511 z"
                id="prefix__path2844-8-1"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 90.46213,93.60522 c -7.9716,1.3183 -15.50224,-3.8048 -16.82016,-11.4427 -1.31791,-7.638 4.07596,-14.8984 12.04756,-16.2166 7.9716,-1.3183 15.50224,3.8048 16.82016,11.4428 1.31792,7.6379 -4.07596,14.8983 -12.04756,16.2165 z"
                id="prefix__path2844-1-7-6"
                fill="#5e81ac"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
            <path
                d="m 90.77223,92.20802 c -7.83783,1.3182 -15.2421,-3.8049 -16.5379,-11.4428 -1.2958,-7.6379 4.00756,-14.8984 11.84539,-16.2166 7.83783,-1.3183 15.24211,3.8048 16.53791,11.4428 1.29579,7.6379 -4.00757,14.8983 -11.8454,16.2166 z"
                id="prefix__path2844-1-7-6-5"
                fill="#434c5e"
                fillOpacity={1}
                fillRule="nonzero"
                stroke="none"
                imageRendering="auto"
            />
        </g>
    )
}

function GameBoard(props: PropsFromRedux): JSX.Element {
    if (props.gameIsVisible === "Hidden") return <div className={[styles.center, styles.area].join(" ")}/>;
    const spacer = 10;
    const margin = 80;
    const playerWidth = 30
    const mapWidth = getCanvasWidth(margin, spacer, playerWidth, props.players.length)
    const mapHeight = 500;
    const mapSize = {height: mapHeight, width: mapWidth}
    const pollutionLevel = props.game.pollutionLevel
    const players = props.players.map((player, i) => drawPlayer(player, i, mapHeight, spacer, playerWidth, margin,props.currentEpoch, props.config))

    return (
        <div className={styles.line}>
            <div className={styles.help}>
                <h2>Objectif : Baisser la ligne horizontale (violette) au niveau du sol.</h2>

                Vous formez une équipe où chacun contrôle sa barre verte de soutien.

                <h2>Consignes</h2>

                <ul>
                    <li><span className={styles.em}>Démarrage</span> : cliquez sur votre barre</li>
                    <li><span className={styles.em}>Mouvement</span> : utilisez les flèches du clavier (⬆️, ⬇️).</li>
                    <li><span className={styles.em}>Blocage</span> : une barre qui n'est plus en contact avec la ligne violette trop longtemps bloque le jeu.</li>
                    <li><span className={styles.em}>Déblocage</span> : revenez toucher la ligne violette pour débloquer le groupe.</li>
                </ul>

                <b>Mode expert</b> : Vous avez réussi ? Bravo ! Saurez-vous faire mieux ?

            </div>
            <div className={[styles.area].join(" ")} onKeyDown={props.move} tabIndex={-1} onClick={props.clicked}>
                <svg width={mapWidth} height={mapHeight} className={styles.area} id={"gameMap"}>
                    {drawGrid(mapSize)}
                    {drawPollutionLevel(mapSize, pollutionLevel)}
                    {sun(pollutionLevel, mapHeight)}
                    {clouds(pollutionLevel, mapSize)}
                    {players}
                    {drawZeroPollution(mapSize)}
                </svg>
            </div>
            <ScoreBoard/>
        </div>
    )
}

export default connector(GameBoard)