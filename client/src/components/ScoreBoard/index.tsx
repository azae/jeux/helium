import {State} from "../../state";
import {connect, ConnectedProps} from "react-redux";
import {Dispatch} from "redux";
import {HeliumAction} from "../../reducer";
import React from "react";
import {Colors, level2label, Levels} from "../../domain/config";
import styles from "./index.module.css"

function mapStateToProps(state: State) {
    return {
        scores: state.scoreBoard,
    }
}

function mapDispatchScoreBoard(dispatch: Dispatch<HeliumAction>) {
    return {}
}

const connector = connect(mapStateToProps, mapDispatchScoreBoard)

type PropsFromRedux = ConnectedProps<typeof connector>

function ScoreBoard(props: PropsFromRedux) {
    const colors = [Colors.ONE, Colors.TWO, Colors.THREE];
    const levels = [Levels.EASY, Levels.MEDIUM, Levels.HARD];

    function getScoreForLevelAndColor(l: Levels, c: Colors) {
        return props.scores.filter((e) => e.level === l && e.colors === c).reduce((acc, e) => {
            return acc + e.score
        }, "");
    }

    const levelsLines: string[][] = levels.map(l => [level2label(l)].concat(colors.map(c => getScoreForLevelAndColor(l, c))))

    console.log("levelsLines: " + levelsLines)
    return (
        <div>
            <h2>Meilleurs scores</h2>
            <table className={styles.table}>
                <tr>
                    <td></td>
                    <td>1 couleur</td>
                    <td>2 couleurs</td>
                    <td>3 couleurs</td>
                </tr>
                {levelsLines.map((line) => {
                    return (
                        <tr>{line.map(cell => {
                            return <td>{cell}</td>
                        })}</tr>
                    )
                })}
            </table>
        </div>
    )
}

export default connector(ScoreBoard)