package net.azae.games.helium.api;

import net.azae.games.helium.domain.Game;
import net.azae.games.helium.websocket.Helper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GameResourceTest {

    @Test
    public void should_create_game() {
        GameResource gameResource = new GameResource();
        Game game = gameResource.create();
        assertEquals(4, game.getId().length());
    }

    @Test
    public void should_read_game_details() {
        GameResource gameResource = new GameResource();
        Game game = gameResource.create();
        game.addPlayer(Helper.samplePlayer("player 1", 45));
        Game actual = gameResource.read(game.getId());
        assertEquals(game, actual);
    }
}