package net.azae.games.helium.websocket;

import org.junit.jupiter.api.Test;

import javax.websocket.Session;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameWebSocketServerUnitTest {

    @Test
    public void should_extract_all_game_sessions() {
        GameWebSocketServer webSocketServer = new GameWebSocketServer();
        Session sessionPlayer1 = new MockSession("player 1");
        Session sessionPlayer2 = new MockSession("player 2");
        webSocketServer.open(sessionPlayer1);
        webSocketServer.open(sessionPlayer2);
        GameMessage message = new GameMessage();
        message.setGameId("My Game");
        webSocketServer.handleMessage(message, sessionPlayer1);
        webSocketServer.handleMessage(message, sessionPlayer2);
        assertEquals(2, webSocketServer.extractGameSessions("My Game").size());
    }

    @Test
    public void should_extract_only_game_sessions() {
        GameWebSocketServer webSocketServer = new GameWebSocketServer();
        Session sessionPlayer1 = new MockSession("player 1");
        Session sessionPlayer2 = new MockSession("player 2");
        webSocketServer.open(sessionPlayer1);
        webSocketServer.open(sessionPlayer2);
        GameMessage message = new GameMessage();
        message.setGameId("Game 1");
        webSocketServer.handleMessage(message, sessionPlayer1);
        message.setGameId("Game 2");
        webSocketServer.handleMessage(message, sessionPlayer2);
        assertEquals(1, webSocketServer.extractGameSessions("Game 1").size());
    }
}