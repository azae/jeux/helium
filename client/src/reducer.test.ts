import {Colors, Levels} from "./domain/config";
import {updateScoreBoard} from "./reducer";

describe("reducer", () => {
    describe("updateScoreBoard", () => {
        it("should add an entry for level 0", () => {
            expect(updateScoreBoard(Levels.EASY, 0, 212, [], Colors.ONE))
                .toEqual([{level: Levels.EASY, score: 212, colors: Colors.ONE}])
        })
        it("should update an entry for level 0", () => {
            expect(updateScoreBoard(Levels.EASY, 0, 212, [{level: Levels.EASY, score: 12, colors: Colors.ONE}], Colors.ONE))
                .toEqual([{level: Levels.EASY, score: 212, colors: Colors.ONE}])
        })
        it("should return actual score board when level is greater than 0", () => {
            expect(updateScoreBoard(Levels.EASY, 200, 212, [], Colors.ONE))
                .toEqual([])
        })
        it("should return actual score board when game level is training", () => {
            expect(updateScoreBoard(Levels.TRAINING, 0, 212, [], Colors.ONE))
                .toEqual([])
        })
    })
})