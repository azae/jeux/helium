export const INITIAL_SIZE = 200

export type PlayerTs = {
    name: string,
    size: number,
    locked: boolean,
    isSuffocate: boolean,
    startSuffocate: number
}
export type PlayerJava = {
    name: string,
    size: number,
}

export function updatePlayerFromJava(java: PlayerJava, node:PlayerTs): PlayerTs {
    return {
        name: java.name,
        size: java.size,
        locked: node.locked,
        isSuffocate: node.isSuffocate,
        startSuffocate: node.startSuffocate,
    }
}

export function newPlayerFromJava(java: PlayerJava): PlayerTs {
    return {
        name: java.name,
        size: java.size,
        locked: false,
        isSuffocate: false,
        startSuffocate: 0,
    }
}

export function newPlayer(name: string): PlayerTs {
    return {
        name: name,
        size: 0,
        locked: false,
        isSuffocate: false,
        startSuffocate: 0,
    }
}

export function convertPlayerToJava(player:PlayerTs ): PlayerJava {
    return {
        name: player.name,
        size: player.size,
    }
}