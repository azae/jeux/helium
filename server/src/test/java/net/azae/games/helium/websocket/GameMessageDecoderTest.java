package net.azae.games.helium.websocket;

import org.junit.jupiter.api.Test;

import javax.websocket.DecodeException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameMessageDecoderTest {
    @Test
    public void should_decode_full_GameMessage() throws DecodeException {
        GameMessage expected = Helper.sampleMessage("Game name", 200, Helper.samplePlayer("player", 132));
        GameMessage actual = new GameMessageDecoder().decode("" +
                "{" +
                "\"gameId\": \"Game name\"," +
                "\"score\": \"200\"," +
                "\"player\": {\"name\": \"player\", \"size\": \"132\"}" +
                "}");
        assertEquals(expected, actual);
    }

    @Test
    public void should_decode_GameMessage_without_player() throws DecodeException {
        GameMessage expected = Helper.sampleMessage("Game name", 200, null);
        GameMessage actual = new GameMessageDecoder().decode("" +
                "{" +
                "\"gameId\": \"Game name\"," +
                "\"score\": \"200\"" +
                "}");
        assertEquals(expected, actual);
    }

}