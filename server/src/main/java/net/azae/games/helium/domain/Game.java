package net.azae.games.helium.domain;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private static final int INITIAL_SCORE = 1000;
    private String id;
    private List<Player> players = new ArrayList<>();
    private int score= INITIAL_SCORE;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
