import {State} from "../../state";
import {Dispatch} from "redux";
import {HeliumAction} from "../../reducer";
import {connect, ConnectedProps} from "react-redux";
import style from "./index.module.css"
import React, {ChangeEvent} from "react";
import {Colors, colors2string, level2string, Levels, string2Colors, string2Level} from "../../domain/config";

function mapStateToProps(state: State) {
    return {
        config: state.config
    }
}

function mapDispatchNewGame(dispatch: Dispatch<HeliumAction>) {
    return {
        changeLockDelay: (e: ChangeEvent<HTMLInputElement>) => {
            dispatch({type: "UPDATE_CONFIGURATION_LOCK_DELAY", value: e.target.value})
        },
        changeMaxMove: (e: ChangeEvent<HTMLInputElement>) => {
            dispatch({type: "UPDATE_CONFIGURATION_MAX_MOVE", value: e.target.value})
        },
        changePenalityFactor: (e: ChangeEvent<HTMLInputElement>) => {
            dispatch({type: "UPDATE_CONFIGURATION_PENALIZE_FACTOR", value: e.target.value})
        },
        updateColors: (v: ChangeEvent<HTMLSelectElement>) => {
            dispatch({type: "UPDATE_COLORS", value: string2Colors(v.target.value)})
        },
    }
}

const connector = connect(mapStateToProps, mapDispatchNewGame)
type PropsFromRedux = ConnectedProps<typeof connector>


function buildOptions() {
    return Object.entries(Colors)
        .map(([k, v]) => <option value={k}>{v}</option>)
}

function GameConfig(props: PropsFromRedux) {
    if (!props.config.show) return (<></>)
    return (<>
        <div className={style.card}>
            <div className={style.cardLabel}>Lock delay</div>
            <div className={style.cardInfo}>
                <input
                    type="text"
                    className={style.cardInput}
                    value={props.config.lockDelay}
                    onChange={props.changeLockDelay}
                    size={props.config.lockDelay.toString().length}
                />
            </div>
        </div>
        <div className={style.card}>
            <div className={style.cardLabel}>Max move size</div>
            <div className={style.cardInfo}>
                <input
                    type="text"
                    className={style.cardInput}
                    value={props.config.maxMove}
                    onChange={props.changeMaxMove}
                    size={props.config.maxMove.toString().length}
                />
            </div>
        </div>
        <div className={style.card}>
            <div className={style.cardLabel}>Penalize factor</div>
            <div className={style.cardInfo}>
                <input
                    type="text"
                    className={style.cardInput}
                    value={props.config.penaliseFactor}
                    onChange={props.changePenalityFactor}
                    size={props.config.penaliseFactor.toString().length}
                />
            </div>
        </div>
        <div className={style.card}>
            <div className={style.cardLabel}>Couleurs</div>
            <div className={style.cardInfo}>
                <select className={style.select} onChange={props.updateColors} value={colors2string(props.config.colors)}>
                    {buildOptions()}
                </select>
            </div>
        </div>
    </>)
}

export default connector(GameConfig)