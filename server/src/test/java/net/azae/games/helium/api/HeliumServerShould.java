package net.azae.games.helium.api;

import net.azae.games.helium.domain.Game;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import javax.ws.rs.core.Application;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class HeliumServerShould extends JerseyTest {

    @Override
    protected Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        // set(TestProperties.CONTAINER_PORT, "50080");
        return new Api();
    }

    @Test
    public void have_a_game_lifecycle() {
        Game game = target("games").request().buildPost(null).invoke(Game.class);
        List games = target("games").request().get(List.class);
        assertAll(
                () -> assertThat(game).isNotNull(),
                () -> assertThat(games).extracting("id").contains(game.getId())
        );
    }

    @Test
    public void players_propagate_their_moves_to_others() {
        /**
         * https://stackoverflow.com/questions/37686934/can-i-test-websockets-with-jersey-test-framework
         * I guess the answer is no....
         */
    }
}
