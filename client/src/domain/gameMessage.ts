import {PlayerJava} from "./player";
import {send} from "@giantmachines/redux-websocket/dist";
import {GameConfigJava} from "./config";

export type GameMessage = {
    player: PlayerJava;
    score: number;
    gameId: string;
    config: GameConfigJava
}

export function sendMessage(msg: GameMessage) {
    console.log("send Message")
    console.log(msg)
    return send(msg)
}